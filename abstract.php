<?php
    abstract class AbstractClass{
        abstract public function getValue():int;
        public function printValue(){
            echo 'Переданное значение '.$this->getValue();
        }
    }

    class Value extends AbstractClass{
        public $value;
        public function getValue():int{
            return $this->value;
        }
    }
    $value = new Value();
    $value->value = 8;
    $value->printValue();
?>