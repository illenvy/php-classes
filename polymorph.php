<?php
    class A{
        private $x;
        public function __construct(int $x){
            $this->x = $x;
        }
        public function getValue(){
            return $this->x;
        }
    }
    class B extends A{
        
    }
    $a = new A(7);
    $b = new B(8);
    var_dump($a instanceof B);
?>