<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        class Cat{
            private $name;
            private $color;
            
            public function __construct(string $name, string $color){
                $this->name = $name;
                $this->color = $color;
            }
            public function setName(string $name){
                $this->name = $name;
            }
            public function getName(){
                return $this->name;
            }
            public function getColor(){
                return $this->color;
            }

            public function sayHello(){
                return 'Меня зовут '.$this->name;
            }
        }
        $cat1 = new Cat('Barsik', 'Black');
        // $cat1->setName('barsik');
        echo $cat1->getColor();
        echo $cat1->sayHello();
        // var_dump($cat1);
    ?>
</body>
</html>