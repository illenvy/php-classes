<?php

    interface calcSquare{
        public function calcSquare():float;
    }
    class Rectangle implements calcSquare{
        private $x;
        private $y;

        public function __construct(int $x, int $y){
            $this->x = $x;
            $this->y = $y;
        }

        public function calcSquare():float
        {
            return ($this->x*$this->y);
        }
    }

    class Square{
        private $x;

        public function __construct(int $x){
            $this->x = $x;
        }

        public function calcSquare():float
        {
            return ($this->x**2);
        }
    }
    
    class Circle{
        private $r;
        public const PI = 3.14;

        public function __construct(int $x){
            $this->r = $r;
        }

        public function calcSquare():float
        {
            return (self::PI * ($this->r**2));
        }
    }
    $array = [
        $rectangle = new Rectangle(3,5),
        $square = new Square(6),
        $circle = new Circle(5),
    ];
    
    foreach($array as $ob){
        if($ob instanceof calcSquare) echo $$ob->calcSquare();
        else echo 'Объект не подписан на интерфейс';
    }
?>