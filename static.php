<?php
    // class User{
    //     public static $count=0;
    //     private $name;
    //     private $role;
    //     public function __construct(string $role, string $name){
    //         self::$count++;
    //         $this->name = $name;
    //         $this->role = $role;
    //     }
    //     public static function setAdmin(string $name){
    //         return new self('admin', $name);
    //     }
    // }
    // for($i = 0; $i < 7; $i++){
    //     User::setAdmin('Sasha');
    // }
    // // var_dump(User::setAdmin('Sasha'));
    // echo User::$count;

    class User{
        private $name;
        public function __construct(string $name){
            $this->name = $name;
        }
        public function getName():string
        {
            return $this->name;
        }
    }
    class Article{
        private $title;
        private $text;
        private $author;

        public function __construct(string $title, string $text, User $author){
            $this->title = $title;
            $this->text = $text;
            $this->author = $author;
        }
        public function getAuthor()
        {
            return $this->author;
        }
    }
    $user = new User('Sasha');
    $article = new Article('Title', 'Text', $user);
    var_dump($article->getAuthor()->getName());
?>