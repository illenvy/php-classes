<?php
    class Post{
        protected $title;
        protected $text;

        public function __construct(string $title, string $text){
            $this->title = $title;
            $this->text = $text;
        }
        public function getTitle(){
            return $this->title;
        }
        public function getText(){
            return $this->text;
        }
    }
    class Article extends Post{
        private $homework;

        public function __construct(string $title, string $text, string $homework){
            parent::__construct('Title', 'Text');
            $this->homework = $homework;
        }
        public function getHomework(){
            return $this->homework;
        }

        public function getTitle(){
            return 'Заголовок статьи '.$this->title;
        }
    }
    $article = new Article('Title', 'Text', 'Homework');
    echo $article->getTitle();
    // var_dump($article);
?>